include make.inc

src/KolmogorovSmirnovDist.o: src/KolmogorovSmirnovDist.c src/KolmogorovSmirnovDist.h
	$(CC) $(CFLAGS) -o src/KolmogorovSmirnovDist.o -c src/KolmogorovSmirnovDist.c

$(KSLIB): src/KolmogorovSmirnovDist.c src/KolmogorovSmirnovDist.h
	$(CC) $(CFLAGS) -o src/KolmogorovSmirnovDist.o -c src/KolmogorovSmirnovDist.c
	$(AR) $(ARFLAGS) $(KSLIB) src/KolmogorovSmirnovDist.o

clean:
	$(RM) KolmogorovSmirnovDist.o
